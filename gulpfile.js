var gulp = require('gulp'), 
    sass = require('gulp-ruby-sass') 
    notify = require("gulp-notify") 
    bower = require('gulp-bower');
    bs = require('browser-sync').create();

var config = {
     sassPath: './resources/sass',
     bowerDir: './bower_components' 
}

gulp.task('browser-sync', ['css'], function() {
    bs.init({
        server: {
            baseDir: "./public"
        }
    });
});



gulp.task('bower', function() { 
    return bower()
         .pipe(gulp.dest(config.bowerDir)) 
});

gulp.task('icons', function() { 
    return gulp.src(config.bowerDir + '/fontawesome/fonts/**.*') 
        .pipe(gulp.dest('./public/fonts')); 
});

gulp.task('css', function() { 
    return gulp.src(config.sassPath + '/style.scss')
         .pipe(sass({
             style: 'compressed',
             loadPath: [
                 './resources/sass',
                 config.bowerDir + '/bootstrap-sass-official/assets/stylesheets',
                 config.bowerDir + '/fontawesome/scss',
             ]
         }) 
            .on("error", notify.onError(function (error) {
                 return "Error: " + error.message;
             }))) 
         .pipe(gulp.dest('./public/css'))
        .pipe(bs.reload({stream: true}));
});

// Rerun the task when a file changes
 gulp.task('watch', ['browser-sync'], function() {
     gulp.watch(config.sassPath + '/**/*.scss', ['css']).on('change', bs.reload);
    gulp.watch("public/*.html").on('change', bs.reload);
});

// gulp.task('watch', ['browser-sync'], function () {
//     gulp.watch("resources/sass/*.scss", ['css']);
//     gulp.watch("public/*.html").on('change', bs.reload);
// });

  gulp.task('default', ['bower', 'icons', 'css']);
