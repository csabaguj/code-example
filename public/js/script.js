// A $( document ).ready() block.
$( document ).ready(function() {
    $( "#datepicker" ).datepicker();

    //add another child

    $(document).on('click','.add-child', function(){
      var ele = '<div class="child-details"><hr>' +
        '<div class="radios">' +
        '<label class="radio-inline">' +
          '<input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" checked> Boy' +
        '</label>' +
        '<label class="radio-inline">' +
        '<input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> Girl' +
        '</label><i class="fa fa-times" aria-hidden="true"></i>' +
        '</div>' +
        '<select class="form-control">' +
          '<option>Month</option>' +
        '</select>' +
        '<select class="form-control second">' +
          '<option>Year</option>' +
        '</select>' +
        '<div class="add-child"><i class="fa fa-plus" aria-hidden="true"></i><span>Add another child</span></div>' +
      '</div>';

      $(this).closest('.child-details').after(ele);
      $(this).hide();
  })

  $(document).on('click','.fa-times', function(){
    $(this).closest('.child-details').remove();
    var n = $( ".child-details" ).length;
    if (n == 1) {
      $(".add-child").show();
    }
  });
});
